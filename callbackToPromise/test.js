const { readFile } = require('./you-can-change')

describe('Some Module', () => {
    describe('Some Asynchronous function', () => {
        it('should read nothing out of an empty file', (done) => {
            readFile('thisFileShouldNotExist.js').then((fileContents) => {
                expect(fileContents).toBe('')
                done()
            }).catch(done.fail)
        })
    })
})
