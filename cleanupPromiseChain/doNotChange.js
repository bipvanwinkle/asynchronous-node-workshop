function connectDatabase() {
    return Promise.resolve({
        getBooks: () => Promise.resolve(['To Kill a Mockingbird', 'The Tale of Two Cities']),
        getUser: () => Promise.resolve({ firstName: 'Jack', lastName: 'Russell' })
    });
}

function findAllBooks(databaseConnection) {
    return databaseConnection.getBooks();
}

function getCurrentUser(databaseConnection) {
    return databaseConnection.getUser();
}

function pickTopRecommendation(books, { firstName, lastName }) {
    const randomIndex = Math.round(Math.random() * (books.length - 1));
    return `${firstName} ${lastName} would love to read ${books[randomIndex]}`
}

module.exports = {
    connectDatabase,
    findAllBooks,
    getCurrentUser,
    pickTopRecommendation
};
