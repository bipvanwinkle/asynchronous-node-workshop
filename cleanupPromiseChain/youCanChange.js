const {
    connectDatabase,
    findAllBooks,
    getCurrentUser,
    pickTopRecommendation
} = require('./doNotChange');

connectDatabase().then(database => {
    return findAllBooks(database).then(books => {
        return getCurrentUser(database).then(user => {
            console.log(pickTopRecommendation(books, user))
        });
    });
});
