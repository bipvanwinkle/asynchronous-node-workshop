const { readFile } = require('./helpers')

describe('Some Module', () => {
    describe('Some Asynchronous function', () => {
        it('should read nothing out of an empty file', () => {
            return readFile('callbackToPromise/text.txt').then((fileContents) => {
                expect(fileContents).toBe('The contents of this file are now yours to behold' + '\n') })
        })
    })
})
