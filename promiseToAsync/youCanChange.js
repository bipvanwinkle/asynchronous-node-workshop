const { readFile, writeFile, getJoke, getRandomJoke } = require('./helpers');

function main() {
    return Promise.all([getJoke(), getRandomJoke()])
        .then(([joke, randomJoke]) => {
            return writeFile('joke.txt', joke).then(() => {
                return writeFile('joke.txt', '\n' + randomJoke, { flag: 'a' });
            });
        })
        .then(() => {
            readFile('joke.txt').then(jokes => {
                console.log(jokes);
            });
        });
}

main().catch(console.error);
