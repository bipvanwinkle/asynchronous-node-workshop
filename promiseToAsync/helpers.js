const fs = require('fs')
const axios = require('axios')

function writeFile (path, data, options) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, options, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve()
            }
        })
    })
}

function readFile (path, encoding = 'utf8') {
    return new Promise((resolve, reject) => {
        fs.readFile(path, encoding, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

async function getJoke (jokeId = 42) {
    const { data } = await axios.get(`https://api.icndb.com/jokes/${jokeId}`)
    return data.value.joke
}

async function getRandomJoke () {
    const { data } = await axios.get('https://api.icndb.com/jokes/random?limitTo=[nerdy]')
    return data.value.joke
}

module.exports = {
    readFile,
    getJoke,
    getRandomJoke,
    writeFile
}
