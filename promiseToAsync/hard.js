//1) Change this function to use AWS Lambda's promise API
//2) Change this function to use async/await

export function processFromApi(event, context, callback) {
    const eventBody = parseBody(event, callback);

    if (!eventBody) return; // Checks for invalid JSON, error already handled in parseBody

    const data = _.get(eventBody, 'data');
    const amount = _.get(eventBody, 'data.amount');
    const isAuthOnly = _.get(eventBody, 'isAuthOnly'); //force auth
    const currency = _.get(data, 'currency');
    let paymentMethod = _.get(data, 'paymentMethod');
    paymentMethod = ensureCreditCardPaymentMethodIsNowCard(paymentMethod);
    const methodMap = _.get(processMethodMap, `[${paymentMethod}]`, 10);
    const card = _.get(eventBody, 'card');
    const processingOptions = _.get(eventBody, 'processingOptions', {
        checkFraud: true
    });
    let tokenRecord;
    eventBody.clientIp = _.get(event, 'requestContext.identity.sourceIp');

    let missing = validateInput(eventBody, [
        'tokenex.token',
        'data.amount'
    ]);

    if (!_.isEmpty(missing)) {
        return customErrors({ requiredFieldsError: true }, callback, `Missing required values. [${missing.join(', ')}]`);
    }

    if (!_.isFinite(_.toNumber(amount))) {
        return customErrors({ invalidAmountError: true }, callback, 'Amount value is not a number');
    }

    checkMerchantIdsWriteAccess(event).then((merchantIds) => {
        if (_.isEmpty(merchantIds)) {
            return Promise.reject({ accessRightsError: true, message: 'Insufficient access rights, user must be an administrator' });
        }

        return getMerchantWithBestGateway(merchantIds, currency, methodMap);
    }).then(async (merchant) => {
        tokenRecord = {
            merchant,
            data,
            card,
            processingOptions,
            tokenex: merchant.tokenex,
            paymentMethod
        };
        tokenRecord = await addCardToTokenRecord(eventBody, tokenRecord);
        return processPayment(eventBody, tokenRecord, { isAuthOnly });
    }).then((response) => {
        return saveTransaction(eventBody, tokenRecord, response.gatewayResponse, response.kountResults);
    }).then((utt) => {
        success(callback, utt);
    }).catch((error) => {
        customErrors(error, callback, 'Cannot process payment, please check your request');
    });
}

